// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    css: [
        '@/assets/contents.css'
    ],
    components : [
        {
            path:'~/components',
            extensions:['.vue'],
        }
    ]
})
